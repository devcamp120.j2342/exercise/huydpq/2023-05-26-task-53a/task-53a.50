import models.Account;

public class App {
    public static void main(String[] args) throws Exception {
        Account account1 = new Account("1", "Account 1");
        Account account2 = new Account("2", "Account 2", 1000);

        System.out.println(account1.toString());
        System.out.println(account2.toString());

        //  tăng cho tài khoản 1 thêm 2000 và cho tài khoản 2 thêm 3000
        account1.credit(2000);
        account2.credit(3000);

        System.out.println("\nThông tin tài khoản sau khi tăng số dư:");
        System.out.println(account1.toString());
        System.out.println(account2.toString());

        // giảm tài khoản 1 đi 1000 và tài khoản 2 đi 5000
        account1.debit(1000);
        account2.debit(5000);

        System.out.println("\nThông tin tài khoản sau khi giảm số dư:");
        System.out.println(account1.toString());
        System.out.println(account2.toString());

        // chuyển 2000 từ tài khoản 1 đến tài khoản 2
        account1.transferTo(account2, 2000);

        System.out.println("\nThông tin tài khoản sau khi chuyển khoản từ tài khoản 1 sang tài khoản 2:");
        System.out.println(account1.toString());
        System.out.println(account2.toString());

        // chuyển 2000 từ tài khoản 2 đến tài khoản 1
        account2.transferTo(account1, 2000);

        System.out.println("\nThông tin tài khoản sau khi chuyển khoản từ tài khoản 2 sang tài khoản 1:");
        System.out.println(account1.toString());
        System.out.println(account2.toString());

    }
}
